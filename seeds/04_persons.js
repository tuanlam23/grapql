exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('persons').truncate()
    .then(function () {
      // Inserts seed entries
      retkurn knex('persons').insert([
        {name: "lam", age: "18", gender: "male", created_at: knex.fn.now(), updated_at: knex.fn.now()},
        {name: "hung", age: "20", gender: "male", created_at: knex.fn.now(), updated_at: knex.fn.now()},
      ]);
    });
};
